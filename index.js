'use strict';

function TestCasesToolBuilder() {
    this.suite = [];
    this.scenario = [];
    this.steps = [];
    this.apiKey = null;
    this.url = null;
}

TestCasesToolBuilder.prototype.config = function (config) {
    this.apiKey = config.apiKey;
    this.url = config.url || "localhost:3000";
};

TestCasesToolBuilder.prototype.startSuite = function () {
    const shell = require('shelljs');
    this.suite = {
        branch: shell.exec("git rev-parse --abbrev-ref HEAD", {silent:true}).trim(),
        commit: shell.exec("git log -1 --pretty=format:\"%H\"", {silent:true}).trim(),
        time: new Date().toISOString(),
        api_key: this.apiKey
    };
};

TestCasesToolBuilder.prototype.startScenario = function () {
    this.steps = [];
    this.scenario = this.scenario || [];
};

TestCasesToolBuilder.prototype.step = function (step) {
    this.steps.push(step)
};

TestCasesToolBuilder.prototype.buildScenario = function (scenario, steps=null) {
    this.scenario.push({
        directory: scenario.directory,
        feature: scenario.feature,
        scenario: scenario.scenario,
        steps: steps ? steps : this.steps,
        duration: scenario.duration,
        status: scenario.status,
        location: scenario.location,
        exception: scenario.exception
    });
};

TestCasesToolBuilder.prototype.stopSuite = function (status, duration) {
    let merge = require('merge');
    this.suite = merge(this.suite,
        {   status: status,
            duration: duration,
            scenarios: this.scenario
        });
    if (this.apiKey) {
        this.sendData();
    } else {
        console.log("This is a local run or API_KEY isn't exists")
    }
};

TestCasesToolBuilder.prototype.sendData = function () {
    let request = require('request');
    request.post({
            url: this.url + "/api/parse_report",
            json: this.suite
        }, function(error, res, body) {
            if (body.status == "ok") {
                console.log(`Report is uploaded! You can check it on ${res.socket._httpMessage._headers.host}/reports/${body.report.id}?project_id=${body.report.project_id}`)
            } else {
                console.log(`Something going wrong on upload report :(\n${body.report.message}\nFile saved here #{file}`)
            }
        }
    );
};

module.exports = TestCasesToolBuilder;